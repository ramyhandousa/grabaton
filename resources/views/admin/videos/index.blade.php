@extends('admin.layouts.master')

@section('title', 'الإعلانات')
@section('styles')

    <!-- Custom box css -->
    <link href="{{ request()->root() }}/public/assets/admin/plugins/custombox/dist/custombox.min.css" rel="stylesheet">




    <style>
        .errorValidationReason{

            border: 1px solid red;

        }
    </style>
@endsection
@section('content')

    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15 ">
                <a href="{{ route('videos.create') }}" type="button" class="btn btn-custom waves-effect waves-light"
                   aria-expanded="false">
                <span class="m-l-5">
                <i class="fa fa-plus"></i>
                </span>
                    إضافة فيديو
                </a>
            </div>
            <h4 class="page-title">إدارة الفديوهات </h4>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <div class="dropdown pull-right">
                    {{--<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">--}}
                    {{--<i class="zmdi zmdi-more-vert"></i> --}}
                    {{--</a>--}}

                </div>

                <h4 class="header-title m-t-0 m-b-30">
                    {{--@lang('trans.managers_system')--}}
                </h4>

                <table id="datatable-fixed-header" class="table table-striped table-bordered dt-responsive nowrap"
                       cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>إسم الفديو</th>
                        <th>إسم القسم</th>
                        <th>@lang('trans.status' ) </th>
                        {{--<th>مفعل</th>--}}
                        <th>@lang('trans.created_at')</th>
                        <th>@lang('trans.options')</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($videos as $row)
                        <tr>
                            <td>{{  $row->name }}</td>
                            <td>{{ optional( $row->category)->name }}</td>



                            <td>
                                <div class="StatusActive{{ $row->id }}"  style="display: {{ $row->is_suspend == 0 ? "none" : "block" }}; text-align: center;">
                                    <img  width="23px" src="{{ request()->root() }}/public/assets/admin/images/false.png" alt="">
                                </div>
                                <div class="StatusNotActive{{ $row->id }}" style="display: {{ $row->is_suspend == 0 ? "block" : "none" }};  text-align: center;">
                                    <img width="23px" src="{{ request()->root() }}/public/assets/admin/images/ok.png" alt="">
                                </div>

                            </td>

                            <td>{{ $row->created_at != ''? @$row->created_at->format('Y/m/d'): "--" }}</td>
                            <td>

                                <a href="{{ route('videos.show', $row->id) }}"
                                   data-toggle="tooltip" data-placement="top"
                                   data-original-title="@lang('institutioncp.show_details')"
                                   class="btn btn-icon btn-xs waves-effect  btn-info">
                                    <i class="fa fa-eye"></i>
                                </a>

                                <a href="{{ route('videos.edit',$row->id) }}"
                                   class="btn btn-icon btn-xs waves-effect btn-warning m-b-5">
                                    <i class="fa fa-edit"></i>
                                </a>

                                <a href="javascript:;" data-id="{{ $row->id }}" data-type="0"
                                   data-url="{{ route('videos.suspend') }}"  style="@if($row->is_suspend == 0) display: none;  @endif"
                                   class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill  suspendElement suspend{{ $row->id }}"
                                   id="suspendElement" data-message="تاكيد التفعيل"
                                   data-toggle="tooltip" data-placement="top"
                                   title="" data-original-title="فك الحظر ">
                                    <i class="fa fa-unlock"></i>
                                </a>

                                <a href="javascript:;" data-id="{{ $row->id }}" data-type="1"
                                   data-url="{{ route('videos.suspend') }}" style="@if($row->is_suspend == 1) display: none;  @endif"
                                   class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill suspendElement unsuspend{{ $row->id }}"
                                   id="suspendElement"
                                   data-message="حظر"
                                   data-toggle="tooltip" data-placement="top"
                                   title="" data-original-title="{{ __('trans.suspend') }}">
                                    <i class="fa fa-lock"></i>
                                </a>

                                <a href="javascript:;" id="elementRow{{ $row->id }}" data-id="{{ $row->id }}"
                                   class="removeElement btn btn-icon btn-trans btn-xs waves-effect waves-light btn-danger m-b-5">
                                    <i class="fa fa-remove"></i>
                                </a>


                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div><!-- end col -->
    </div>
    <!-- end row -->


@endsection


@section('scripts')


    <script>
        $('body').on('click', '.removeElement', function () {

            var id = $(this).attr('data-id');
            var $tr = $(this).closest($('#elementRow' + id).parent().parent());
            swal({
            title: "هل انت متأكد؟",
            text: "",
            type: "error",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "موافق",
            cancelButtonText: "إلغاء",
            confirmButtonClass: 'btn-danger waves-effect waves-light',
            closeOnConfirm: true,
            closeOnCancel: true,
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                type: 'POST',
                url: '{{ route('videos.delete') }}',
                data: {id: id},
                dataType: 'json',
                success: function (data) {
                // $('#catTrashed').html(data.trashed);
                if (data.status == true) {
                var shortCutFunction = 'success';
                var msg = 'لقد تمت عملية الحذف بنجاح.';
                var title = data.title;
                toastr.options = {
                positionClass: 'toast-top-center',
                onclick: null,
                showMethod: 'slideDown',
                hideMethod: "slideUp",

                };
                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                $toastlast = $toast;
                $tr.find('td').fadeOut(1000, function () {
                $tr.remove();
                });

                }else{
                var shortCutFunction = 'error';
                var msg = data.message;
                var title = data.title;
                toastr.options = {
                positionClass: 'toast-top-center',
                onclick: null,
                showMethod: 'slideDown',
                hideMethod: "slideUp",

                };
                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                $toastlast = $toast;
                }


                }
                });
                } else {

                    swal({
                title: "تم الالغاء",
                text: "انت لغيت عملية الحذف تقدر تحاول فى اى وقت :)",
                type: "error",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "موافق",
                confirmButtonClass: 'btn-info waves-effect waves-light',
                closeOnConfirm: false,
                closeOnCancel: false

                });

                }
            });

        });

    </script>

@endsection



