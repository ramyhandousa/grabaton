@extends('admin.layouts.master')
@section('title', 'تفاصيل الفديو')
@section('content')





    <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-15">
                    {{--<button type="button" class="btn btn-custom  waves-effect waves-light"--}}
                        {{--onclick="window.history.back();return false;"> @lang('maincp.back') <span class="m-l-5"><i--}}
                                {{--class="fa fa-reply"></i></span>--}}
                    {{--</button>--}}

                    <a href="{{ route('videos.index') }}"
                       class="btn btn-custom  waves-effect waves-light">
												<span><span>رجوع  </span>
													<i class="fa fa-reply"></i>
												</span>
                    </a>

                </div>
                <h4 class="page-title">بيانات الفديو </h4>
            </div>
        </div>


        <div class="row">


                <div class="col-sm-12">

                <div class="card-box">
                    <div class="row">

                        <div class="col-lg-12">
                            <div class="card-box p-b-0">


                                <h4 class="header-title m-t-0 m-b-30">التفاصيل</h4>

                                <form>
                                    <div id="basicwizard" class=" pull-in">
                                        <ul class="nav nav-tabs navtab-wizard nav-justified bg-muted">
                                            <li class="active"><a href="#tab1" onclick="pauseVid()" data-toggle="tab" aria-expanded="false">البيانات الاساسية</a></li>
                                            <li class=""><a href="#tab2" data-toggle="tab" aria-expanded="true"> الفيديو </a></li>
                                        </ul>
                                        <div class="tab-content b-0 m-b-0">
                                            <div class="tab-pane m-t-10 fade active in" id="tab1">
                                                <div class="row">
                                                    <div class="col-sm-8">
                                                        <div class="col-xs-12 col-lg-12">

                                                            <hr>
                                                        </div>

                                                        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                            <label> إسم الفديو :</label>
                                                            <input class="form-control" value="{{ $video->name }}"><br>
                                                        </div>

                                                        @if($video->description )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12" style="margin: 10px auto">
                                                                <label> تفاصيل الفديو  :</label>
                                                                <textarea class="form-control">{{ $video->description }}</textarea>
                                                            </div>
                                                        @endif



                                                        @if($video->category )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>القسم التابع لهذا الفديو  :</label>
                                                                <input class="form-control" value="{{ optional($video->category)->name  }}"><br>
                                                            </div>
                                                        @endif




                                                    </div>

                                                </div>

                                            </div>
                                            <div class="tab-pane m-t-10 fade " id="tab2">

                                                @if( $video->uploads)
                                                    <div class="form-group">

                                                    @if(count($video->uploads) > 0)

                                                        @foreach($video->uploads as $item)


                                                            <div class="col-lg-12">

                                                                <video id="videoPlayer" width="100%" height="50%" controls>
                                                                    <source src="{{$item['url']}}" type="video/mp4">
                                                                </video>
                                                                {{--<iframe width="420" height="315" src="{{$item['url']}}" frameborder="0" allowfullscreen></iframe>--}}

                                                                {{--<a data-fancybox="gallery">--}}

                                                                   {{--href="{{ $helper->getDefaultImage($item->url, request()->root().'/public/assets/admin/custom/images/default.png') }}">--}}
                                                                    {{--<img style="width: 100% ; height: 100%" src="{{ $helper->getDefaultImage($item->url, request()->root().'/public/assets/admin/custom/images/default.png') }}"/>--}}
                                                               {{----}}
                                                                {{--</a>--}}
                                                            </div>
                                                        @endforeach
                                                    @endif
                                                    </div>
                                                @else
                                                        لا يوجد اي صور لهذا الإعلان
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>



                    </div>
                </div>
            </div>

        </div>




@endsection




@section('scripts')
    <script type="text/javascript">

        var videoplayer = document.getElementById("videoPlayer");

        function pauseVid() {
            videoplayer.pause();
        }


    </script>


@endsection


