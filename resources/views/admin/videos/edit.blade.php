@extends('admin.layouts.master')
@section('title', __('trans.banks_management'))


@section('styles')
    <style>
        {{--position:relative;--}}
    .progress {  width:100%; height: 30px; border: 1px solid #7F98B2; padding: 1px; border-radius: 3px; }
        .bar { background-color: #B4F5B4; width:0%; height:25px; border-radius: 3px; }
        .percent { position:absolute; display:inline-block; top:3px; left:48%; color: #7F98B2;}
    </style>
@endsection
@section('content')



    <form method="POST" action="{{ route('videos.update', $video->id) }}" enctype="multipart/form-data"
          data-parsley-validate novalidate>
    {{ csrf_field() }}

    {{ method_field('PUT') }}


    <!-- Page-Title -->
        <div class="row">
            <div class="col-lg-12  ">
                <div class="btn-group pull-right m-t-15">
                    {{--  <a href="{{ route('users.create') }}" type="button" class="btn btn-custom waves-effect waves-light"
                        aria-expanded="false"> @lang('maincp.add')
                         <span class="m-l-5">
                         <i class="fa fa-plus"></i>
                     </span>
                     </a> --}}
                </div>
                <h4 class="page-title">  تعديل فيديو  </h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12  ">
                <div class="card-box">


                    {{--                    <h4 class="header-title m-t-0 m-b-30">@lang('trans.add_bank')</h4>--}}

                    <div class="row">

                        <div   class="col-xs-8">

                            <div   class="col-xs-12">
                                <div class="form-group">
                                    <label for="userName">إسم القسم الرئيسي*</label>
                                    <select name="categoryId" id="" class="form-control requiredFieldWithMaxLenght"
                                            required
                                    >
                                        <option value="" selected disabled=""> إختر القسم</option>
                                        @foreach($cats as $value)
                                            <option value="{{ $value->id }}" @if($value->id == $video->category_id) selected @endif>{{ $value->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="userName">إسم الفديو*</label>
                                    <input type="text" name="name_ar"
                                           class="form-control requiredFieldWithMaxLenght"
                                           required
                                           value="{{$video->name}}"
                                           placeholder="إسم الفديو..."/>
                                    <p class="help-block" id="error_userName"></p>

                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="userName">إسم الفديو بالإنجليزية*</label>
                                    <input type="text" name="name_en"
                                           class="form-control requiredFieldWithMaxLenght"
                                           required
                                           value="{{optional($video->translate('en'))->name}}"
                                           placeholder="إسم الفديو..."/>
                                    <p class="help-block" id="error_userName"></p>

                                </div>
                            </div>


                            <div class="col-xs-12">
                                <div class="form-group">
                                        <textarea type="text" name="desc_ar" class="form-control m-input  title "
                                                  required
                                                  placeholder="وصف الفديو بالغة العربية    "   >{{ $video->description}}</textarea>

                                </div>
                            </div>



                            <div class="col-xs-12">
                                <div class="form-group">
                                        <textarea type="text" name="desc_en" class="form-control m-input  title "
                                                  required
                                                  placeholder="وصف الفديو بالغة الإنجليزية    "   > {{optional($video->translate('en'))->description}} </textarea>

                                </div>
                            </div>
                        </div>

                        <div  class="col-xs-4 changeElement">
                            @if( $video->uploads)
                                        @foreach($video->uploads as $item)

                                            <video id="videoPlayer" width="100%" height="50%" controls>
                                                <source src="{{$item['url']}}" type="video/mp4">
                                            </video>

                                        @endforeach
                            @endif
                        </div>
                        <div style="display: none" id="checkForVideo"  class="col-xs-4 changeElement">
                            <div class="form-group">
                                <label for="usernames">الفديو *</label>
                                <input type="file"
                                       accept="video/*"
                                       name="file" class="dropify" />
                                <div class="progress">
                                    <div class="bar"></div >
                                    <div class="percent">0%</div >
                                </div>
                            </div>

                        </div>

                        <div class="form-group text-right m-t-20 changeElement">
                            <a onclick="changeVideo()" class="btn btn-warning waves-effect waves-light m-t-20"   >
                                تغير الفيديو
                            </a>
                        </div>

                        <div style="display: none" class="form-group text-right m-t-20 changeElement">
                            <a onclick="changeVideo()" class="btn btn-warning waves-effect waves-light m-t-20 "     >
                                رجوع  للفيديو القديم
                            </a>
                        </div>
                        <!-- end col -->


                    </div>

                    <div class="form-group text-right m-t-20">
                        <button class="btn btn-success waves-effect waves-light m-t-20 hideButton" value="Submit"   type="submit">
                            @lang('maincp.save_data')
                        </button>
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            @lang('maincp.disable')
                        </button>
                    </div>
                </div>




            </div>
        </div><!-- end col -->


        </div>
        <!-- end row -->
    </form>

@endsection



@section('scripts')

    {{--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script>--}}
    <script src="http://malsup.github.com/jquery.form.js"></script>

    <script type="text/javascript"
            src="{{ request()->root() }}/public/assets/admin/js/validate-{{ config('app.locale') }}.js"></script>


    <script type="text/javascript">


        var  checkForVideo = document.getElementById("checkForVideo");


        function changeVideo() {

            $(".changeElement").toggle();

        }


        function validate(formData, jqForm, options) {

            var form = jqForm[0];

                if (!form.file.value && checkForVideo.style.display != 'none'   ) {
                    showErrors('{{ session()->get('errors', 'برجاء اختيار فديو التحميل من فضلك  ') }}');
                    return false;
                }

        }

        (function() {

            var bar = $('.bar');
            var percent = $('.percent');
            var status = $('#status');

            $('form').ajaxForm({
                beforeSubmit: validate,
                beforeSend: function() {
                    $(".hideButton").hide();
                    status.empty();
                    var percentVal = '0%';
                    var posterValue = $('input[name=file]').fieldValue();
                    bar.width(percentVal)
                    percent.html(percentVal);
                },
                uploadProgress: function(event, position, total, percentComplete) {
                    var percentVal = percentComplete + '%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                    $('.loading').show();
                },
                success: function(data) {
                    var percentVal = 'Wait, Saving';
                    bar.width(percentVal)

                    if (data.status == 422 ){

                        $(".hideButton").show();
                        $('.loading').hide();

                        var shortCutFunction = 'error';
                        var msg = data.message.file;
                        var title = 'خطأ';
                        toastr.options = {
                            positionClass: 'toast-top-center',
                            onclick: null,
                            showMethod: 'slideDown',
                            hideMethod: "slideUp",
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;
                        return false;

                    }
//                    percent.html(percentVal);
                    window.location.href = data.url;
                },
                complete: function(xhr) {

                    if (xhr.status == 200 && xhr.responseJSON.status == 200 ){

                        status.html(xhr.responseText);
                        showMessage('{{ session()->get('success', 'تم التعديل بنجاح') }}');
                        $(".hideButton").show();
                        $('.loading').hide();

                    }

                },error:function (error) {
                    $('.loading').hide();
                    $(".hideButton").show();
                    showErrors('{{ session()->get('errors', 'حدث خطا اثناء الرفع ') }}');

                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                }
            });

        })();
    </script>
@endsection

