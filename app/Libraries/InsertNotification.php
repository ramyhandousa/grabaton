<?php
namespace App\Libraries;

use App\Libraries\PushNotification;
use App\Models\Device;
use  App\Models\Notification;

class InsertNotification {

    public $push;
    function __construct(PushNotification $push)
    {
        $this->push = $push;
    }

    public  function NotificationDbType($type  ,$user,  $sender = null ,  $request = null  , $ads = null ,$additional = null){

            $admins= Device::whereDeviceType('web')->pluck('user_id');
            $devicesWeb= Device::whereDeviceType('web')->pluck('device');
             $userIos= Device::where('user_id','!=',$sender)->whereDeviceType('Ios')->pluck('user_id');

            switch($type){

            case $type == 1:

                // contactUs Form admin
                //  $sender  admin who send
                // admin panel
                  $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender,
                    'title' =>   trans('global.connect_us')  ,
                    'body' => $request ,
                    'type' => 1,
                  ];
                  $this->insertData($data);
             break;

            case $type == 2:

                     // contactUs Form Users
                  foreach ($admins as $admin){
                         $data = [
                           'user_id' => $admin ,
                           'sender_id' => $sender ,
                           'title' => trans('global.connect_us'),
                           'body' => $request ,
                           'type' => 2,
                         ];
                         $this->insertData($data);
                  }
                $this->push->sendPushNotification(null, $devicesWeb, 'تواصل معنا', $request);
                  break;

            case $type == 3:

            // want To Send Price To Ads
                $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender->id,
                    'ads_id' => $ads,
                    'title' =>   'سوم الإعلانات'  ,
                    'body' => $request.' بتقديم سوم بمبلغ ' . $sender->name . ' قام المستخدم ' ,
                    'type' => 3,
                ];
                $this->insertData($data);

             break;

            case $type == 4:

                $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender->id,
                    'ads_id' => $ads,
                    'title' =>   'سوم الإعلانات'  ,
                    'body' =>  ' قام صاحب الإعلان بقبول الطلب الخاص بك علي اسم الإعلان ' .$request,
                    'type' => 4,
                ];
                $this->insertData($data);
             break;

            case $type == 5:

                $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender->id,
                    'ads_id' => $ads,
                    'title' =>   'سوم الإعلانات'  ,
                    'body' =>  ' قام صاحب الإعلان برفض الطلب الخاص بك علي اسم الإعلان ' .$request,
                    'type' => 5,
                ];
                $this->insertData($data);

             break;

            case $type == 6:

                // Bank Transfer For User
                // accepted
                // $user [ user or provider ]
                //  $sender admin panel
              $data = [
                'user_id' => $user ,
                'sender_id' => $sender ,
                'title' => trans('global.bank_transfers'),
                'body' =>    trans('global.bank_transfers_accepted')   ,
                'type' => 6,
              ];
              $this->insertData($data);

             break;
            case $type == 7:

                // Bank Transfer For User
                // refuse
                // $user [ user or provider ]
                //  $sender admin panel
                  $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender ,
                    'title' => trans('global.bank_transfers'),
                    'body' =>    trans('global.bank_transfers_refuse')  ,
                    'type' => 7,
                  ];
                  $this->insertData($data);

             break;
            case $type == 8:

                // contactUs Form Users
                foreach ($admins as $admin){
                    $data = [
                        'user_id' => $admin ,
                        'sender_id' => $sender ,
                        'ads_id' => $ads,
                        'title' => 'تقارير الإعلانات',
                        'body' => $request ,
                        'type' => 8,
                    ];
                    $this->insertData($data);
                }
                $this->push->sendPushNotification(null, $devicesWeb, 'تقارير الإعلانات', $request);
                break;

             break;
            case $type == 9:

                    // contactUs Form Users
                    foreach ($admins as $admin){
                        $data = [
                            'user_id' => $admin ,
                            'sender_id' => $sender ,
                            'title' => $request->commentId .' الابلاغ عن تعليق رقم ',
                            'body' => $request->message ,
                            'type' => 9,
                        ];
                        $this->insertData($data);
                    }
                 break;

             case $type == 21:

                    $data = [
                        'user_id' => $user->user_id ,
                        'sender_id' => $sender->id,
                        'ads_id' => $ads,
                        'title' =>   ' التعليقات '  ,
                        'body' =>  '  قام بالتعليق علي إعلانك ' .$sender->name,
                        'type' => 21,
                    ];
                    $this->insertData($data);

                break;

             break;

            default:

     }


    }


    private function insertData($data)
    {
     if (count($data) > 0) {

         $time = ['created_at' => now(),'updated_at' => now()];
           Notification::insert($data   + $time);

     }
    }

}
       
       