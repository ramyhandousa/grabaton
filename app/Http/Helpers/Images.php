<?php

namespace App\Http\Helpers;

/**
 * Created by PhpStorm.
 * User: hassan saeed
 * Date: 7/25/2017
 * Time: 4:24 PM
 */
use Image;

use getID3;
class Images
{


    /**
     * RETURN path to save in images tables DATABASE
     * @RETURN IMAGE PATH
     *
     * SAVE MAIN IMAGE
     */
    public static function uploadMainImage($request, $name, $path = null)
    {

        if ($request->hasFile($name)):
            // Get File name from POST Form
            $image = $request->file($name);

            // Custom file name with adding Timestamp
            $filename = time() . '.' . $image->getClientOriginalName();

            // Directory Path Save Images
            $path = public_path($path . $filename);

            // Upload images to Target folder By INTERVENTION/IMAGE
            $img = Image::make($image);

            $img->save($path);

            // RETURN path to save in images tables DATABASE
            return $filename;
        endif;
    }

    /**
     * RETURN path to save in images tables DATABASE
     * @RETURN IMAGE PATH
     *
     * SAVE MAIN IMAGE
     */
    public static function uploadSubImages($request, $name, $path = null)
    {

        if ($request->hasFile($name)):
            // Get File name from POST Form
            $image = $request->file($name);

            // Custom file name with adding Timestamp
            $filename = time() . '.' . str_random(20) . $image->getClientOriginalName();

            // Directory Path Save Images
            $path = public_path($path . $filename);

            // Upload images to Target folder By INTERVENTION/IMAGE
            $img = Image::make($image);

            $img->save($path);

            // RETURN path to save in images tables DATABASE
            return $filename;
        endif;
    }

    /**
     * RETURN path to save in images tables DATABASE
     * @RETURN IMAGE PATH
     *
     * SAVE THUMBNAILS IMAGES
     */
    public static function uploadThumbImage($request, $name, $path = null, $width = null, $height = null)
    {
        if ($request->hasFile($name)):
            // Get File name from POST Form
            $image = $request->file($name);

            // Custom file name with adding Timestamp
            $filename = time() . '.' . $image->getClientOriginalName();

            // Directory Path Save Images
            $path = public_path($path . $filename);

            // Upload images to Target folder By INTERVENTION/IMAGE
            $img = Image::make($image);

            // RESIZE IMAGE TO CREATE THUMBNAILS
            $img->resize($width, $height, function ($ratio) {
                $ratio->aspectRatio();
            });
            $img->save($path);

            // RETURN path to save in images tables DATABASE
            return $filename;
        endif;
    }


    /**
     * RETURN path to save in images tables DATABASE
     * @RETURN IMAGE PATH
     *
     * SAVE THUMBNAILS IMAGES
     */
    public static function uploadImage($request, $name, $path = null, $width = null, $height = null)
    {
        if ($request->hasFile($name)):
            // Get File name from POST Form
            $image = $request->file($name);

            // Custom file name with adding Timestamp
            $filename = time() . '.' . str_random(20) . $image->getClientOriginalName();


            // Directory Path Save Images
            $path = public_path($path . $filename);

            // Upload images to Target folder By INTERVENTION/IMAGE
            $img = Image::make($image);

            // RESIZE IMAGE TO CREATE THUMBNAILS
            if (isset($width) || isset($height))
                $img->resize($width, $height, function ($ratio) {
                    $ratio->aspectRatio();
                });
            $img->save($path);

            // RETURN path to save in images tables DATABASE
            return $filename;
        endif;
    }

    public static function uploadVideo($request , $name, $pathInPublic){

        if ($request->hasFile($name)):

//            $myUpload = explode('.', $_FILES[$name]["name"]);
//            $fileName = current($myUpload);
//            $extension = end($myUpload);

            $file = $request->file($name);
            $filename = time() . '.' . $file->getClientOriginalName();
            $path = public_path().$pathInPublic ;
            $file->move($path, $filename);


            return $filename;

        endif;
    }

    public static function informationVideo($name ,$index = null){

        if ($name ):

            $getID3 = new getID3();
            $video_file = $getID3->analyze($_FILES[$name]['tmp_name']);

            $myUpload = explode('.', $_FILES[$name]["name"]);
            $nameOfVideo = current($myUpload);

            switch ($video_file) {
                case $index == 'mime_type' :
                    $type = $video_file['mime_type'];
                    break;

                case $index == 'fileSize' :
                    $type = number_format($video_file['filesize'] / 1048576,0);
                    break;

                case $index == 'quality' :
                    $type =  $video_file['video']['resolution_y'];
                    break;

                case $index == 'duration' :
                    $type = $video_file['playtime_string'];
                    break;

                case $index == 'nameOfVideo' :
                    $type = $nameOfVideo;
                    break;

                case $index == 'extension' :
                    $type = $video_file['fileformat'];
                    break;

                default:
                    $type = $video_file['filename'];
            }
            return $type;

        endif;

    }


    public function getDefaultImage($image, $defaultImagePath)
    {
        return ($image != '') ? $image : $defaultImagePath;
    }



//    public static function list_categories(Array $categories)
//    {
//        $data = [];
//
//        foreach($categories as $category)
//        {
//            $data[] = [
//                'comment' => $category->comment,
//                'children' => list_categories($category->children),
//            ];
//        }
//
//        return $data;
//    }


    /**
     * Converts numbers in string from western to eastern Arabic numerals.
     *
     * @param  string $str Arbitrary text
     * @return string Text with western Arabic numerals converted into eastern Arabic numerals.
     */
//    public static function arabic_w2e($str)
//    {
//        if(config('app.locale') == 'ar'):
//            $arabic_eastern = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
//            $arabic_western = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
//            return str_replace($arabic_western, $arabic_eastern, $str);
//    else:
//    $arabic_eastern = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
//    $arabic_western = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
//    return str_replace($arabic_eastern, $arabic_western, $str);
//            endif;
//
//    }

    /**
     * Converts numbers from eastern to western Arabic numerals.
     *
     * @param  string $str Arbitrary text
     * @return string Text with eastern Arabic numerals converted into western Arabic numerals.
     */
//    function arabic_e2w($str)
//    {
//        $arabic_eastern = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
//        $arabic_western = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
//        return str_replace($arabic_eastern, $arabic_western, $str);
//    }

    public static function translate($from_lan, $to_lan, $text)
    {
        $json = json_decode(file_get_contents('https://ajax.googleapis.com/ajax/services/language/translate?v=1.0&q=' . urlencode($text) . '&langpair=' . $from_lan . '|' . $to_lan));
        $translated_text = $json->responseData->translatedText;

        return $translated_text;
    }


}
