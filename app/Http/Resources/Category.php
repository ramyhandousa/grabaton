<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Category extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
//    public function toArray($request)
//    {
//        return parent::toArray($request);
//    }

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->when($this->image , $this->image)
        ];

    }

    private function subCategory(){
        $data = [
            'id' => (int) request()->subCategory,
            'name' => 'الكل'
        ];
        return $data;
    }

//    public function with($request)
//    {
//        return [
//            'meta' => [
//                'key' => 'value',
//            ],
//        ];
//    }

    public function withResponse($request, $response)
    {
        $request->keys = 'sasa';
    }
}
