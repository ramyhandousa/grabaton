<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class uploadResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->url,
            'fileExtension' => $this->when($this->fileExtension , $this->fileExtension) ,
            'fileType' => $this->when($this->fileType , $this->fileType ) ,
            'fileSize' => $this->when($this->fileSize , $this->fileSize) ,
            'fileQuality' => $this->when($this->fileQuality , $this->fileQuality) ,
            'timeVideo' => $this->when($this->time , $this->time) ,
            'fileDescription' => $this->when($this->fileDescription , $this->fileDescription) ,
        ];
    }
}
