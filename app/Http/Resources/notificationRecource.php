<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class notificationRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->when($this->title , $this->title) ,
            'body' => $this->when($this->body , $this->body) ,
            'is_read' =>  $this->is_read ,
            'time'     => $this->when($this->created_at , date('h:i A', strtotime($this->created_at))),
        ];
    }
}
