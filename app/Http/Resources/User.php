<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
//    public function toArray($request)
//    {
//        return parent::toArray($request);
//    }

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->when($this->email , $this->email) ,
            'image' => $this->when($this->image , $this->image) ,
            'is_active' => $this->is_active,
            'is_suspend' => $this->is_suspend,
            'api_token' => $this->api_token,
            'message' => $this->when($this->message, $this->message) ,
        ];
    }

}
