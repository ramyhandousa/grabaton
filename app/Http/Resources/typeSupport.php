<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class typeSupport extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->types_support_id,
            'name' => $this->name,
        ];
    }
}
