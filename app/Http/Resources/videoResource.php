<?php

namespace App\Http\Resources;

use App\Models\Upload;
use Illuminate\Http\Resources\Json\JsonResource;

class videoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->when($this->description , $this->description) ,
            'category' =>  $this->when($this->category_id , new Category($this->category)),
            'videos' => $this->when($this->uploads , $this->getViedoWithIds($this->uploads)) ,
            'user' => new User($this->whenLoaded('user')),
            'who_add_video' => $this->add_by,
            'rate' =>  $this->when($this->ratings , number_format($this->averageRating,2)),
            'views' => $this->when($this->views , count($this->views)) ,
        ];
    }


    private function getViedoWithIds($uploads){
        $data = Upload::whereIn('id',$uploads)->get();

        $filter = uploadResource::collection($data);
        return $filter;
    }
}
