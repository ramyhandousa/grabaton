<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Setting;
use App\Models\Upload;
use App\Models\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use getID3;
use UploadImage;
use Validator;
class VideoController extends Controller
{

    public $public_path;

    function __construct()
    {
        $this->public_path = 'files/videos/';

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $videos = Video::latest()->get();

        return view('admin.videos.index',compact('videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $cats = Category::whereParentId(0)->where('is_suspend',0)->get();

        return view('admin.videos.create' , compact('cats'));
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:mp4,ogx,oga,ogv,ogg,webm',
        ]);

        if ($validator->fails()) {
            $valErrors = $validator->messages();
            return response()->json([  'status' => 422 ,'message' => $valErrors]);
        }



//        $ids =  Video::where('user_id',Auth::id())->pluck('uploads');
//
//        $MB_User_Have = Upload::whereIn('id', $ids)->pluck('fileSize')->sum();
//
//        $max_upload =  Setting::whereKey('mx_upload_video')->first()? value('body') : null;
//
//        if ($max_upload){
//            $getID3 = new getID3();
//            $video_file = $getID3->analyze($_FILES['file']['tmp_name']);
//
//
//            $userUploadAndMaxHave =  number_format($video_file['filesize'] / 1048576,0) + $MB_User_Have;
//
//            if ($userUploadAndMaxHave > $max_upload) {
//                $valErrors = ['file' => trans('global.permission_max_upload')];
//                return response()->json([  'status' => 422 ,'message' => $valErrors]);
//            }
//        }


        DB::transaction(function ( ) use ($request) {

            DB::beginTransaction();

            $video = new Video();
            $video->user_id = \Auth::id();
            $video->{'name:ar'} = $request->name_ar;
            $video->{'name:en'} = $request->name_en;
            $video->{'description:ar'} = $request->desc_ar;
            $video->{'description:en'} = $request->desc_en;
            $video->category_id = $request->categoryId;
            $video->save();

            $getID3 = new getID3();
            $video_file = $getID3->analyze($_FILES['file']['tmp_name']);
            $myUpload = explode('.', $_FILES['file']["name"]);
            $nameOfVideo = current($myUpload);

            $upload = new Upload();
            $upload->url = $request->root() . '/public/' . $this->public_path . UploadImage::uploadVideo($request, 'file','/files/videos/');
            $upload->fileExtension = $video_file['fileformat'];
            $upload->fileType = $video_file['mime_type'];
            $upload->fileSize = number_format($video_file['filesize'] / 1048576,0);
            $upload->fileQuality = $video_file['video']['resolution_y'];
            $upload->time = $video_file['playtime_string'];
            $upload->fileDescription = $nameOfVideo;
            $upload->save();

            $video->update(['uploads' => (array) json_encode(  $upload->id)  ]);

            if( !$video || !$upload )
            {
                DB::rollBack();

            } else {

                DB::commit();

            }

        });

        return response()->json([  'status' => 200 ,'url' => route('videos.index')]);

    }

    private function uploadVideoFile($request){
        $myUpload = explode('.', $_FILES["file"]["name"]);
        $fileName = current($myUpload);
        $extension = end($myUpload);
        $file_size = number_format($request->file('file')->getSize() / 1048576,0);

        $upload = new Upload();
        $upload->url = $request->root() . '/public/' . $this->public_path . UploadImage::uploadVideo($request, 'file','/files/videos/');
        $upload->fileExtension = $extension;
        $upload->fileType = $_FILES["file"]["type"] ;
        $upload->fileSize = $file_size ;
        $upload->fileQuality = $file_size ;
        $upload->fileDescription = $fileName ;
        $upload->save();

    }




    public function show($id)
    {
        $video = Video::with('user','category')->findOrFail($id);
        $video->uploads =   $video->uploads ?$this->getFileUploads( $video->uploads) : null;

        return view('admin.videos.show' , compact('video'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $video = Video::with('user','category')->findOrFail($id);
        $video->uploads =   $video->uploads ?$this->getFileUploads( $video->uploads) : null;

        $cats = Category::whereParentId(0)->where('is_suspend',0)->get();

        return view('admin.videos.edit' , compact('video','cats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $video = Video::findOrFail($id);
        $video->user_id = \Auth::id();
        $video->{'name:ar'} = $request->name_ar;
        $video->{'name:en'} = $request->name_en;
        $video->{'description:ar'} = $request->desc_ar;
        $video->{'description:en'} = $request->desc_en;
        $video->category_id = $request->categoryId;
        $video->save();

        if ($request->file){
            $getID3 = new getID3();
            $video_file = $getID3->analyze($_FILES['file']['tmp_name']);
            $myUpload = explode('.', $_FILES['file']["name"]);
            $nameOfVideo = current($myUpload);

            $upload = new Upload();
            $upload->url = $request->root() . '/public/' . $this->public_path . UploadImage::uploadVideo($request, 'file','/files/videos/');
            $upload->fileExtension = $video_file['fileformat'];
            $upload->fileType = $video_file['mime_type'];
            $upload->fileSize = number_format($video_file['filesize'] / 1048576,0);
            $upload->fileQuality = $video_file['video']['resolution_y'];
            $upload->fileDescription = $nameOfVideo;
            $upload->save();

            $video->update(['uploads' => (array) json_encode(  $upload->id)  ]);

        }

        return response()->json([  'status' => 200 ,'url' => route('videos.index')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function suspend(Request $request)
    {
        $model = Video::findOrFail($request->id);
        $model->is_suspend = $request->type;
        if ($request->type == 1) {
            $message = "لقد تم حظر  بنجاح";

        } else {
            $message = "لقد تم فك الحظر بنجاح";
        }

        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type

            ]);
        }

    }

    public function delete(Request $request)
    {

        $video = Video::findOrFail($request->id);




        if ($video) {
            $video->delete();
            $video->deleteTranslations();

            return response()->json([
                'status' => true,
                'data' => [
                    'id' => $request->id
                ],
                'message' => 'لقد تم عمليه الحذف بنجاح'
            ]);
        }


    }

    private function getFileUploads($images){
        $image = Upload::whereIn('id',$images)->select('id','url','fileDescription')->get();

        return $image;
    }

}
