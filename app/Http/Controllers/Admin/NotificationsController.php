<?php

namespace App\Http\Controllers\Admin;


use App\Models\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications;
use App\User;
use App\Models\Device;
use App\Libraries\Main;
use App\Libraries\PushNotification;
use Carbon\Carbon;

class NotificationsController extends Controller
{


    public $push;
    public $main;

    public function __construct(PushNotification $push, Main $main)
    {

        $this->push = $push;
        $this->main = $main;

        Carbon::setLocale(config('app.locale'));

    }

    public function index()
    {
         
        // $notifications = auth()->user()->notifications;
        $notifications = auth()->user()->notifications()->orderBy('created_at', 'desc')->get();


        // $notifications->map(function ($q) {


        //     $notifyData = $this->localizationNotification($q->type, $q->order_id);
        //     $q->type = (int)$q->type;
        //     $q->senderImage = $this->getSenderImage($q->sender_id);
        //     $q->title = $notifyData['title'];
        //     $q->body = $notifyData['body'];

        // });


        // return $notifications;

        return view('admin.notifications.index')->with(compact('notifications'));
    }


    private function localizationNotification($type, $orderId = null)
    {


        $type = (int)$type;

        $data = [];
        switch ($type) {
            case $type == 3:
                $data['title'] = __('trans.newOrderTitle', ['id' => $orderId]);
                $data['body'] = __('trans.newOrderBody');
                break;

            default:
                $data['title'] = "default title";
                $data['body'] = "default body";
        }

        return $data;

    }

    private function getSenderImage($id)
    {
        $user = User::findOrFail($id);

        if (!$user)
            return "";

        $image = "";

        if ($user->userType() == 'company') {
            $image = $user->company_logo;
        } else {

            $image = $user->image;
        }

        return $image == null ? "" : $image;


    }


    /**
     * Remove User from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {

        $model = auth()->user()->notifications()->whereId($request->id)->first();


        if ($model->delete()) {
            return response()->json([
                'status' => true,
                'data' => $request->id
            ]);
        }


    }


    /**
     * Remove User from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function groupDelete(Request $request)
    {


        $ids = $request->ids;

        foreach ($ids as $id) {
            $model = auth()->user()->notifications()->whereId($id)->first();
            $model->delete();
        }


        return response()->json([
            'status' => true,
            'data' => [
                'id' => $request->id
            ]
        ]);
    }


    public function publicNotifications()
    {

        $notifications = Notification::whereType(7)->get();
        return view('admin.notifications.list')->with(compact('notifications'));

    }


    public function createPublicNotifications()
    {
        return view('admin.notifications.public');
    }


    public function sendPublicNotifications(Request $request)
    {

        $query = User::whereIsSuspend(0)->whereIsActive(1);
        if ($request->usersType == "companies") {
            $query->whereIsUser(1);
        }
        if ($request->usersType == "clients") {
            $query->whereIsUser(3);
        }

        if ($request->usersType == "drivers") {
            $query->whereIsUser(2);
        }


        if ($request->city != "" && $request->city != "all") {
            $query->whereCityId($request->city);
        }

        $users = $query->get();

        $this->sendNotificationByLang($request, 7, $users->where('lang', 'ar')->values(), 'ar');
        $this->sendNotificationByLang($request, 7, $users->where('lang', 'en')->values(), 'en');


        session()->flash('success', __('trans.successSendPublicNotifications'));
        return redirect()->back();

    }

    private function sendNotificationByLang($request, $type, $users, $lang)
    {

        $data = array(
            'title' => $lang == "ar" ? "الشاحنات" : "Oil Trips",
            'body' => $lang == "ar" ? $request->notification_message : $request->notification_message_en,
            'type' => $type,
        );

        $notificationData = [];
        $devices = [];

        foreach ($users as $user) {
            $notificationData[] = array(
                'user_id' => $user->id,
                'title' => $data['title'],
                'body' => $data['body'],
                'order_id' => null,
                'type' => $data['type'],
                'sender_id' => auth()->id(),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            );

            foreach ($user->devices as $device) {
                $devices[] = $device;
            }
        }
         $this->main->insertData(Notification::class, $notificationData);

        $webDevice = collect($devices)->where('device_type', 'web')->pluck('device');
        
        $this->push->sendPushNotification([], $webDevice, $data['title'], $data['body'], $data);
    }


    public function getNotifications()
    {
        $user = auth()->user();
        $notifications = $user->notifications;
        return view('admin.notifications.index')->with(compact('notifications'));

    }


}
