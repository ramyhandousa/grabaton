<?php

namespace App\Http\Controllers\Admin;

use App\Models\Bank;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use UploadImage;
use Illuminate\Support\Facades\Gate;

class BanksController extends Controller
{
    /**
     * @var Category
     */

    public $public_path;

    public function __construct()
    {
        $this->public_path = 'files/banks/';
    }


    public function index()
    {
        $banks = Bank::get();
        return view('admin.banks.index')->with(compact('banks'));
    }


    public function create()
    {
        return view('admin.banks.create');
    }

    public function edit($id)
    {
        $bank = Bank::findOrFail($id);
        if (!$bank)
            return abort(404);
        return view('admin.banks.edit')->with(compact('bank'));
    }


    public function store(Request $request)
    {

        $postData = [
//            'name' => $request->name,
            'account_number' => $request->account_number,
            'iban_number' => $request->iban_number,

        ];

        // Declare Validation Rules.
        $valRules = [
//            'name' => 'required',
            'account_number' => 'required',
            'iban_number' => 'required',
        ];

        // Declare Validation Messages
        $valMessages = [
//            'name.required' => 'اسم البنك مطلوب',
            'account_number.required' => 'رقم الحساب مطلوب',
            'iban_number.required' => 'رقم الحساب مطلوب',
        ];

        // Validate Input
        $valResult = Validator::make($postData, $valRules, $valMessages);

        // Check Validate
        if ($valResult->passes()) {


            $model = new Bank();
            $model->{'name:ar'} = $request->name_ar;
            $model->{'name:en'} = $request->name_en;
            $model->account_number = $request->account_number;
            $model->iban_number = $request->iban_number;
            $model->is_active = 1;

            if ($request->hasFile('image')):
                $model->image = $request->root() . '/public/' . $this->public_path . UploadImage::uploadImage($request, 'image', $this->public_path);
            endif;

            if ($model->save()) {

                return response()->json([
                    'status' => true,
                    "message" => __('trans.addingSuccess',['itemName' => __('trans.bank_account')]),
                    "url" => route('banks.index')
                ]);

            }
        } else {
            // Grab Messages From Validator
            $valErrors = $valResult->messages();
            // Error, Redirect To User Edit
            return redirect()->back()->withInput()
                ->withErrors($valErrors);
        }

    }

    public function update(Request $request, $id)
    {

        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        $model = Bank::findOrFail($id);

        // Get Input

        $postData = [
            'name' => $request->name,
            'account_number' => $request->account_number,
            'iban_number' => $request->ibanNumber,

        ];

        // Declare Validation Rules.
        $valRules = [
            'name' => 'required',
            'account_number' => 'required',
            'iban_number' => 'required',
        ];

        // Declare Validation Messages
        $valMessages = [
            'name.required' => 'اسم البنك مطلوب',
            'account_number.required' => 'رقم الحساب مطلوب',
            'iban_number.required' => 'رقم الايبان مطلوب',
        ];


        // Validate Input
        $valResult = Validator::make($postData, $valRules, $valMessages);

        // Check Validate
        if ($valResult->passes()) {


            $model->name = $request->name;
            $model->account_number = $request->account_number;
            $model->iban_number = $request->ibanNumber;

            /**
             * @ Store Image With Image Intervention.
             */

            if ($request->hasFile('image')):
                $model->image = $request->root() . '/public/' . $this->public_path . UploadImage::uploadImage($request, 'image', $this->public_path);
            endif;

            $model->save();

//            session()->flash('success', 'لقد تم تعديل نوع الطلب بنجاح.');
//            return redirect()->route('products.index');

            return response()->json([
                'status' => true,
                "message" => __('trans.editSuccess',['itemName' => __('trans.bank_account')]),
                "url" => route('banks.index')
            ]);

        } else {
            // Grab Messages From Validator
            $valErrors = $valResult->messages();
            // Error, Redirect To User Edit
            return redirect()->back()->withInput()
                ->withErrors($valErrors);
        }
    }



    public function suspend(Request $request)
    {
        $model = Bank::findOrFail($request->id);
        $model->is_active = $request->type;
        if ($request->type == 1) {
            $message = "لقد تم فك الحظر على البنك بنجاح";
        } else {
            $message = "لقد تم حظر البنك  بنجاح";
        }

        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type
            ]);
        }

    }
}
