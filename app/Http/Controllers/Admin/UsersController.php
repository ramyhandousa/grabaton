<?php

namespace App\Http\Controllers\Admin;

use App\Models\Image;
use App\User;
use Silber\Bouncer\Database\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use UploadImage;
use Validator;

class UsersController extends Controller
{


    public $public_path;

    public function __construct()
    {
        $this->public_path = 'files/users/';
    }
    
    public function index(Request $request)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        $users = User::whereDoesntHave('roles')->whereDoesntHave('abilities')->latest()->get();
        
        
        return view('admin.users.index', compact('users'));

    }


  
    public function create()
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        return view('admin.users.create');
    }

  
    public function store(Request $request)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        

    }
 
    public function show($id)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        
        $user = User::with('ads')->findOrFail($id);



        return view('admin.users.show', compact('user', 'roles'));
    }
    


    public function edit($id)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        return view('admin.users.edit');
    }

   
    public function update(Request $request, $id)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        
    }
    
    public function destroy($id)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
       
    }
    
    
    public function accpetedUser(Request $request){
        $user = User::findOrFail($request->id);
        
        if ($user){
            $user->update(['is_accepted' => 1]);
            return response()->json( [
                'status' => true ,
            ] , 200 );
            
        }else{
            
            return response()->json( [
                'status' => false
            ] , 200 );
            
        }
        
    }


    


}
