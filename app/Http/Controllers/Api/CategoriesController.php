<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Ads;
use App\Models\Ad;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function getCategory() {

        $data = \App\Http\Resources\Category::collection(Category::whereIsSuspend(0)->get());

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );

    }
}
