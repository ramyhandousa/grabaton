<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\bank;
use App\Http\Resources\typeSupport;
use App\Libraries\InsertNotification;
use App\Models\BankTransfer;
use App\Models\Setting;
use App\Models\Support;
use App\Models\TypesSupport;
use App\Models\TypesSupportTranslation;
use App\User;
use UploadImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    public $public_path;
       public $headerApiToken;
       public $language;
        public $notify;
       
       public function __construct( InsertNotification $notification)
       {
	    $this->language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
	    app()->setLocale($this->language);
	    
	    // api token from header
	    $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';

        $this->notify = $notification;
           $this->public_path = 'files/transfers';
       }
       
   public function aboutUs(){


	return response()->json( [
	        'status' => 200 ,
	        'data' => Setting::whereKey('about_us_' . $this->language)->first()->body
	] , 200 );
   }
   



   public function terms(){

       $terms =  Setting::whereKey('terms_' . $this->language)->first()->body ;

	return response()->json( [
	        'status' => 200 ,
	        'data' => $terms ,
	] , 200 );
   }


    public function listBanks(){

        $data = bank::collection(\App\Models\Bank::where('is_active',1)->get());

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }


    public function contactUs(Request $request){
        // check for user
        $user = User::where( 'api_token' , $this->headerApiToken )->first();
        if ( ! $user ) {   return $this->UserNotFound();  }

        if ($request->typeId){

            $type = TypesSupport::whereId($request->typeId)->first();

            if ($type){
                $support = new Support();
                $support->user_id = $user->id;
                $support->type_id = (int) $request->typeId;
                $support->name = $user->name;
                $support->phone = $user->phone;
                $support->message = $request->message;
                $support->save();

                $this->notify->NotificationDbType(2,null,$user->id,$request->message);

                return response()->json( [
                    'status' => 200 ,
                    'message' => trans('global.message_was_sent_successfully'),
                ] , 200 );
            }

        }

    }

    public function bankTransfer(Request $request){

        $user = User::where( 'api_token' , $this->headerApiToken )->first();
        if ( ! $user ) {   return $this->UserNotFound();  }

        $bank = new BankTransfer();
        $bank->user_id =  $user->id;
        $bank->userName =  $request->userName;
        $bank->userAccount =  $request->userAccount;
        $bank->iban =  $request->iban;
        $bank->money =  $request->money;
        $bank->bank =  $request->bankId;
        if ( $request->hasFile( 'image' ) ):
            $bank->image =  $request->root() . '/public/' . $this->public_path . UploadImage::uploadImage( $request , 'image' , $this->public_path );
        endif;
        $bank->save();
        $user->update(['payment' => 1]);

        return response()->json( [
            'status' => 200 ,
            'message' => trans('global.operation_successfully'),
        ] , 200 );
    }


    public function getTypesSupport(){

        $types = TypesSupportTranslation::wherelocale($this->language)->get();

        $data = typeSupport::collection($types);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }


    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }
   
}
