<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\uploadResource;
use App\Http\Resources\videoResource;
use App\Models\Upload;
use App\Models\Video;
use App\Models\View;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use willvincent\Rateable\Rating;

class VideoController extends Controller
{

    public $headerApiToken;

    public function __construct( )
    {
        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
        app()->setLocale($language);


        // api token from header
        $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';

    }

    public function index(Request $request){

        $videos = Video::whereIsSuspend(0);


        // category of video
            if ($request->categoryId){

                $videos->whereCategoryId($request->categoryId);

            }

        // who add video
            if ($request->addBy ){

                $videos->whereAddBy($request->addBy);

            }

        $data = videoResource::collection($videos->with('user')->get());

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }

    public function getById(Request $request){

        $video = Video::whereIsSuspend(0)->whereId($request->videoId)->first();

        if ( ! $video ) {   return $this->VideoNotFound();  }

        if ($this->headerApiToken != ' ' && $this->headerApiToken) {

            $user = User::whereApiToken($this->headerApiToken)->first();

            $exist = View::where(['user_id' => $user->id, 'video_id' => $video->id])->first();

            if (!$exist) {
                $view = new View();
                $view->user_id = $user->id;
                $view->video_id = $video->id;
                $view->save();
            }

        }

        $data = new videoResource($video);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }

    public function myVideos(Request $request){
        $user = User::whereApiToken($this->headerApiToken)->first();

        if ( ! $user ) {   return $this->UserNotFound();  }


        $videos = Video::whereIsSuspend(0)->whereUserId($user->id)->get();

        $data = videoResource::collection($videos);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }

    public function rateVideo(Request $request){

        $video = Video::whereId($request->videoId)->first();


        $user = User::where( 'api_token' , $this->headerApiToken )->first();
        if ( ! $user ) {   return $this->UserNotFound();  }

        $rating = new Rating();
        $userRatingBefore = $rating->where('rateable_id', $video->id)->where('user_id', $user->id)->first();

        if ($userRatingBefore) {
            return response()->json([
                'status' => 400,
                'error' => (array) trans('global.video_rate_before'),
            ]);
        }

        $rating->rating = $request->rateValue;
        $rating->user_id = $user->id;
        $video->ratings()->save($rating);

        return response()->json( [
            'status' => 200 ,
            'message' =>    trans('global.rate_success'),
        ] , 200 );
    }


    public function storeVideo(Request $request){
        $user = User::whereApiToken($this->headerApiToken)->first();

        $video = new  Video();
        $video->name = $request->name;
        $video->description = $request->description;
        $video->user_id = $user->id;
        $video->category_id = (int) $request->categoryId;
        $video->add_by = 'users';

        // ids from upload Images
        if ($request->ids){
            $myids =  implode(',',$request->ids) ;
            $filterIds = explode(',',$myids);
            $video->uploads = $filterIds;
        }

        $video->save();



        $data = new videoResource($video);

        return response()->json( [
            'status' => 200 ,
            'message' => trans('global.video_added'),
            'data' => $data ,
        ] , 200 );

    }

    public function editVideo(Request $request){

        $user = User::whereApiToken($this->headerApiToken)->first();

        $video = Video::whereId($request->videoId)->first();
        if ( ! $video ) {   return $this->VideoNotFound();  }

        $video->name = $request->name ? $request->name : $video->name;
        $video->description = $request->description ?$request->description: $video->description;
        $video->user_id = $user->id;
        $video->category_id = (int) $request->categoryId? (int) $request->categoryId:$video->category_id;
        $video->add_by = 'users';

        // ids from upload Images
        if ($request->ids){
            $myids =  implode(',',$request->ids) ;
            $filterIds = explode(',',$myids);
            $video->uploads = $filterIds;
        }

        $video->save();

        $data = new videoResource($video);

        return response()->json( [
            'status' => 200 ,
            'message' => trans('global.video_edit'),
            'data' => $data ,
        ] , 200 );
    }

    public function delete(Request $request)
    {
        $video = Video::find($request->videoId);

        if ( ! $video ) {   return $this->VideoNotFound();  }

        $video->delete();
        $video->deleteTranslations();

        return response()->json( [
            'status' => 200 ,
            'message' => trans('global.video_delete'),
        ] , 200 );


    }



    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }

    private  function VideoNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) 'هذا الفيديو غير موجود'   ],200);
    }

    private  function categoryNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذا القسم غير موجود'   ],200);
    }


    private function getViedoWithIds($uploads){
        $data = Upload::whereIn('id',$uploads)->get();

        $filter = uploadResource::collection($data);
        return $filter;
    }
}
