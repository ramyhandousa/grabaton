<?php
    
    namespace App\Http\Controllers\api;
    
    use App\Http\Controllers\Controller;
    use App\Http\Resources\Ads;
    use App\Http\Resources\following;
    use App\Http\Resources\notificationRecource;
    use App\Models\Notification;
    use App\Models\Ad;
    use App\Models\Countery;
    use App\Models\Image;
    use App\Models\Setting;
    use App\Models\Upload;
    use App\Models\VerifyUser;
    use App\Models\Video;
    use App\User;
    use Illuminate\Http\Request;
    use Illuminate\Support\Arr;
    use UploadImage;
    use Validator;
    class UsersController extends Controller
    {
    
	 public $public_path;
	 public $defaultImage;
	 public $headerApiToken;
  
	 public function __construct( )
	 {
	        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
	        app()->setLocale($language);
	    
	        $this->public_path = 'files/users/profiles';
	    
	        // default Image upload in profile user
	        $this->defaultImage = \request()->root() . '/' . 'public/assets/images/default-profile-img.png';
	    
	        // api token from header
	        $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';
	    
	 }



	 public function uploadImage(Request $request){

         $user = User::where( 'api_token' , request()->headers->get('apiToken') )->first();

        $ids =  Video::where('user_id',$user->id)->pluck('uploads');

        $MB_User_Have = Upload::whereIn('id', $ids)->pluck('fileSize')->sum();

        $max_upload =  Setting::whereKey('mx_upload_video')->first()? value('body') : null;

        if ($max_upload){
//            $getID3 = new getID3();
//            $video_file = $getID3->analyze($_FILES['file']['tmp_name']);
//
//
//            $userUploadAndMaxHave =  number_format($video_file['filesize'] / 1048576,0) + $MB_User_Have;
//
//            if ($userUploadAndMaxHave > $max_upload) {
//                $valErrors = ['file' => trans('global.permission_max_upload')];
//                return response()->json([  'status' => 400 ,'error' => (array) $valErrors]);
//            }
        }

	      
	        $image = new Upload();
	        $image->url = $request->root() . '/public/files/videos/' . UploadImage::uploadVideo($request, 'file','/files/videos/');
	        $image->save();
	        return response()->json( [
		      'status' => 200 ,
		      'data' => $image ,
	        ] , 200 );
	 }


	 public function editProfile(Request $request){
        
         $user = User::where( 'api_token' , request()->headers->get('apiToken') )->first();
         
         $valResult = Validator::make( $this->DataStudent( $request, $user ) , $this->validData( $user ) , $this->messages());

         if ($valResult->fails()) {
             return response()->json( [ 'status' => 400 , 'error' => $valResult->errors()->all() ] , 200 );
         }


         $action_code = substr(rand(), 0, 4);

         if ($request->phone){
             $checkPhoneChange = $user->wherePhone($request->phone)->count();

             if ($checkPhoneChange == 0){



                 $foundPhone = VerifyUser::whereUserId($user->id)->first();

                 if ($foundPhone){

                     $foundPhone->update(['action_code' => $action_code ]);

                 }else{

                     $verify = new  VerifyUser();
                     $verify->user_id= $user->id;
                     $verify->phone = $request->phone;
                     $verify->action_code = $action_code;
                     $verify->save();

                 }

                 $user->update(['is_active' => 0]);
             }
         }
         
         $user->update($this->DataStudent( $request, $user ));
         $data = new \App\Http\Resources\User($user);

         return response()->json( [
             'status' => 200 ,
             'code' => $request->phone&&$checkPhoneChange == 0 ?  (string)$action_code : '',
             'message' => trans('global.profile_edit_success'),
             'data' => $data ,
         ] , 200 );
         
         }
         
         public function getUser(Request $request){
	        $user  = User::whereId($request->id)
                            ->whereDoesntHave('roles')
                            ->whereDoesntHave('abilities')
                            ->first();
	     
	        if (!$user){  return $this->UserNotFound();  }

           $data = new \App\Http\Resources\User($user);
             return response()->json( [
                 'status' => 200 ,
                 'data' => $data ,
             ] , 200 );
         }


        public function notification(Request $request){
            $user = User::whereApiToken($this->headerApiToken)->first();

            if ( ! $user ) {   return $this->UserNotFound();  }

            $notification = notificationRecource::collection( Notification::whereUserId($user->id)->get());

            return response()->json( [
                'status' => 200 ,
                'data' => $notification,
            ] , 200 );
        }


    
        private function DataStudent ( $request , $user)
        {
            $postData = [
                'name' => $request->name ?$request->name : $user->name ,
                'email' => $request->email ?$request->email : $user->email ,
                'phone' => $request->phone ?$request->phone : $user->phone ,
                'password' => $request->password ,
                'image' => $request->image ? $request->root() . '/public/' . $this->public_path . \UploadImage::uploadImage( $request , 'image' , $this->public_path ): $this->defaultImage ,

            ];

            return $postData;
        }
    
    
        private function validData ( $user )
        {
            $valRules = [ ];
    
            if(\request()->has('phone')){
                $valRules['phone'] = 'unique:users,phone,' . $user->id ;
            }
            if(\request()->has('email')){
                $valRules['email'] = 'unique:users,email,' . $user->id ;
            }
            
            
            return $valRules;
        }
    
        private function messages()
        {
            return [
                'email.unique' => trans('global.unique_email'),
                'phone.unique' => trans('global.unique_phone'),
            ];
        }

        private  function UserNotFound(){
            return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
        }
        private  function CityNotFound(){
            return response()->json([   'status' => 400,  'error' => (array)'المدينة او الدولة غير موجودة'   ],200);
        }

        private  function adNotFoundByUser(){
            return response()->json([   'status' => 401,  'error' => (array) 'هذ الإعلان  لا ينتمي للمستخدم'   ],200);
        }

    }
