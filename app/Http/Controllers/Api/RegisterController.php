<?php

namespace App\Http\Controllers\api;

use App\Http\Helpers\Sms;
use App\Http\Requests\api\resgister;
use App\Models\Countery;
use App\Models\Profile;
use App\Models\VerifyUser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class RegisterController extends Controller
{
       public $public_path;
       public $defaultImage;
       public $headerApiToken;
       
       public function __construct( )
       {
	    $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
	    app()->setLocale($language);
     
	    $this->public_path = 'files/users/profiles';
	    
	    // default Image upload in profile user
	    $this->defaultImage = \request()->root() . '/' . 'public/assets/admin/images/default.png';
	    
	    // api token from header
	    $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';
	    
       }
       
    public function register(resgister $request){
     
	    $action_code = substr( rand(), 0, 4);


        $user = new User();
        $user->name = $request->name;
        $user->email =$request->email;
        $user->phone =$request->phone;
        $user->api_token = str_random(60);
        $user->password = $request->password;
        $user->is_active = 0;
        $user->image = $request->image ? $request->root() . '/public/' . $this->public_path . \UploadImage::uploadImage( $request , 'image' , $this->public_path ): $this->defaultImage;
        $user->save();
        $this->createVerfiy($request , $user , $action_code);

//	    Sms::sendMessage('Activation code:' . $action_code, $request->phone);

        return response()->json( [
            'status' => 200 ,
            'code' => $action_code
        ] , 200 );
    }

   private function createVerfiy($request , $user , $action_code){
       $verifyPhone = new VerifyUser();
       $verifyPhone->user_id = $user->id;
       $verifyPhone->phone =$request->phone;
       $verifyPhone->action_code = $action_code;
       $verifyPhone->save();
   }

  
}
