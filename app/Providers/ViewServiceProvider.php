<?php
/**
 * Created by PhpStorm.
 * User: Hassan Saeed
 * Date: 11/16/2017
 * Time: 9:29 AM
 */

namespace App\Providers;

use App\Libraries\Main;
use App\Models\Category;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;


class ViewServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
 
            $helper = new \App\Http\Helpers\Images();
            $main_helper = new \App\Http\Helpers\Main();
            $setting = new Setting;
            $main = new Main();
            $categoryShared = Category::whereParentId(0)->first();


            $view->with(compact( 'helper', 'main', 'setting','main_helper', 'categoryShared'));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}


