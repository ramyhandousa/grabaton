<?php

namespace App\Models;

use App\User;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use willvincent\Rateable\Rateable;

class Video extends Model
{
    use Rateable;
    use Translatable;

    public $translatedAttributes = ['name', 'description'];

    protected $fillable = ['name', 'description' ,'category_id','uploads','is_suspend','message'];

    protected $casts = [ 'uploads' => 'array'];


    public function category() {
        return $this->belongsTo(Category::class);
    }



    public function user() {
        return $this->belongsTo(User::class);
    }


    public function ratings()
    {
        return $this->morphMany('willvincent\Rateable\Rating', 'rateable');
    }

    public function views() {
        return $this->hasMany(View::class);
    }


}
