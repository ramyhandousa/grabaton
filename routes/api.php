<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group([
    'namespace' => 'Api',
], function () {


    Route::prefix('Auth')->group(function () {

        Route::post('login', 'LoginController@login');
        Route::post('register', 'RegisterController@register');
        Route::post('forgetPassword', 'ForgotPasswordController@forgetPassword');
        Route::post('resetPassword', 'ForgotPasswordController@resetPassword');
        Route::post('checkCode', 'ResetPasswordController@checkCodeActivation');
        Route::post('resendCode', 'ResetPasswordController@resendCode');
        Route::post('changePassword', 'ResetPasswordController@changPassword');
        Route::post('editProfile', 'UsersController@editProfile')->middleware('apiToken');
        Route::post('logOut','LoginController@logOut');

    });

    Route::prefix('videos')->group(function () {

        Route::get('/','VideoController@index');
        Route::get('getById','VideoController@getById');
        Route::get('myVideos','VideoController@myVideos');
        Route::post('rateVideo','VideoController@rateVideo');
        Route::post('storeVideo','VideoController@storeVideo')->middleware('apiToken');
        Route::post('editVideo','VideoController@editVideo')->middleware('apiToken');
        Route::post('deleteVideo','VideoController@delete')->middleware('apiToken');


    });



    Route::prefix('categories')->group(function () {

        Route::get('/','CategoriesController@getCategory');


    });

    Route::prefix('setting')->group(function () {

        Route::get('aboutUs','SettingController@aboutUs');
        Route::post('terms','SettingController@terms');
        Route::get('banks','SettingController@listBanks');
        Route::get('getTypes','SettingController@getTypesSupport');
        Route::post('contactUs','SettingController@contactUs');
        Route::post('bankTransfer','SettingController@bankTransfer');

    });




});


